<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tufactoring Pagina principal del Sistema
 *
 * @author Nelly Moreno
 */
class Noticias extends CI_Controller
{
 public function __construct()
    {
        parent::__construct();
        
        $this->load->helper(array('url', 'form'));
    }
    
    public function index($idioma=1)
    {
        $datos["id_idioma"] = $idioma;
        
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/menu');
        $this->load->view('web/noticias',$datos);
        $this->load->view('cpanel/footer');
    }
    /*
    *   Metodo para ver detalles de noticias
    */
    public function verNoticiaDetalle($idioma=1,$slug_noticia){
        $datos["id_idioma"] = $idioma;
        $datos["slug_noticia"] = $slug_noticia;
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/menu');
        $this->load->view('web/noticias_detalle',$datos);
        $this->load->view('cpanel/footer');
    }
}