<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tufactoring Pagina principal del Sistema
 *
 * @author Nelly Moreno
 */
class Inicio extends CI_Controller
{
 public function __construct()
    {
        parent::__construct();
        
        $this->load->helper(array('url', 'form'));
    }
    public function index($idioma=1)
    {
        $datos["id_idioma"] = $idioma;    
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/menu');
        $this->load->view('web/inicio',$datos);
        $this->load->view('cpanel/footer');
    }
}