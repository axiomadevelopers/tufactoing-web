<!-- Layout-->
<div class="layout">

	<!-- Header-->
	<header class="header header-center undefined">
		<div class="container-fluid">
			<!-- Logos-->
			<div class="inner-header">
				<a class="inner-brand a-menu0"> 
					<img class="brand-dark img-header" src="<?=base_url();?>assets/web/images/logo.png" alt="">
					<img class="brand-light img-header" src="<?=base_url();?>assets/web/images/logo-light.png" alt="">
					<!-- Core-->
				</a>
			</div>
			<!-- Navigation-->
			<div class="inner-navigation collapse">
				<div class="inner-navigation-inline">
					<div class="inner-nav">
						<ul>
							<!-- Home-->
							<li class="">
								<a href="<?=base_url();?>inicio" class="a-menu0">
									<div id="span-menu0" class="span-menu menu0">Inicio</div>
								</a>
							</li>
							<!-- Home end-->
							<!-- Pages-->
							<li class="">
								<a href="<?=base_url();?>quienes_somos" class="a-menu1">
									<div id="span-menu1" class="span-menu menu1">Quienes somos</div>
								</a>
							</li>
							<!-- Pages end-->
							<!-- Portfolio-->
							<li class="">
								<a href="<?=base_url();?>servicios" class="a-menu2">
									<div id="span-menu2" class="span-menu menu2">Servicios</div>
								</a>
							</li>
							<!-- Portfolio end-->
							<!-- Noticias-->
							<li class="">
								<a href="<?=base_url();?>noticias" class="a-menu4">
									<div id="span-menu3" class="span-menu menu4">Noticias</div>
								</a>
							</li>
							<!-- Noticias end-->
							<!-- Blog-->
							<li class="">
								<a href="<?=base_url();?>contactanos" class="a-menu3">
									<div id="span-menu4" class="span-menu menu3">Contactanos
									</div>	
								</a>
							</li>
							<!-- Blog end-->
							<!--Login -->
							<!--<li class="login_registrar">
								<a href="http://admin-tufactoring.000webhostapp.com" class="">
									<div id="" class="span-menu">Login
									</div>	
								</a>
							</li>-->
							
							<!-- -->
							<li>
								<div id="idioma" name="idioma" class="esta_espaniol " title="Versión en Inglés" onclick="cambio_idioma()">
          							<img id="bandera" class="img-flag" src="<?=base_url();?>assets/web/images/flags/uk.png" data="uk">
        						</div>
							</li>
							<!-- -->
							<!-- Login desplegable-->
							<li class="menu-item-has-children"><a href="index.html#">LOGIN</a>
								<ul class="sub-menu">
									<li class="menu-item-has-children">
										<a href="">Opcion 1</a>
										<ul class="sub-menu">
											<li><a href="">sub opcion1</a></li>
											<li><a href="">sub opcion2</a></li>
										</ul>
									</li>
									<li class="menu-item-has-children">
										<a href="">Opcion 2</a>
										<ul class="sub-menu">
											<li><a href="">sub opcion1</a></li>
											<li><a href="">sub opcion2</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<!-- Login desplegable end-->
							<!-- Login desplegable-->
							<li class="menu-item-has-children"><a href="index.html#">REGISTRO</a>
								<ul class="sub-menu">
									<li class="menu-item-has-children">
										<a href="">Opcion 1</a>
										<ul class="sub-menu">
											<li><a href="">sub opcion1</a></li>
											<li><a href="">sub opcion2</a></li>
										</ul>
									</li>
									<li class="menu-item-has-children">
										<a href="">Opcion 2</a>
										<ul class="sub-menu">
											<li><a href="">sub opcion1</a></li>
											<li><a href="">sub opcion2</a></li>
										</ul>
									</li>
								</ul>
							</li>
							<!-- Login desplegable end-->
						</ul>
					</div>
				</div>
			</div>
			<!-- Extra menu-->
			<!--<div class="extra-nav">
				<ul>
					<li><a class="btn-header" href="http://admin-tufactoring.000webhostapp.com" target="_blank"><span class="btn btn-primary ">Login</span></a></li>
				</ul>
			</div>-->
			<!-- Mobile menu-->
			<div class="nav-toggle"><a href="index-21.html#" data-toggle="collapse" data-target=".inner-navigation"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a></div>
		</div>
	</header>