<div name="id_idioma" id="id_idioma" style="display: none"><?php echo $id_idioma;?></div>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>"> 		
		<!-- Footer-->
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-3">
						<!-- Text widget-->
						<aside class="widget widget_text">
							<div class="textwidget">
								<p><img class="imagen-footer" src="<?=base_url();?>assets/web/images/logo-light.png" width="150px" alt=""></p>
								<p id="texto_footer">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat.</p>
							</div>
						</aside>
					</div>
					<div class="col-md-6 col-lg-3">
						<!-- Recent entries widget-->
						<aside class="widget widget_recent_entries">
							<div class="widget-title">
								<h5><span class="" id="footer_company">Servicios</span></h5>
							</div>
							<ul>
								<li><a class="menu1"></a></li>
								<li><a class="menu2"></a></li>
								<li><a class="menu4"></a></li>
							</ul>
						</aside>
					</div>
					
					<div class="col-md-6 col-lg-3">
						<!-- Tags widget-->
						<aside class="widget widget_tag_cloud">
							<div class="widget-title">
								<h5 ><span class="menu2"></span></h5>
							</div>
							<ul>
								<li id="footer-comof1"></li>
								<li id="footer-comof2"></li>
								<li id="footer-comof3"></li>
							</ul>
						</aside>
					</div>
					<div class="col-md-6 col-lg-3">
						<!-- Twitter widget-->
						<aside class="widget twitter-feed-widget">
							<div class="widget-title">
								<h5 ><span class="menu3">Contactanos</span></h5>
							</div>
							<ul>
								<li>
									<a href="mailto:support@tufactoring.com"><i class="fa fa-envelope-o icono-footer" ></i>  contact@tufactoring.com</a> <br/>
								</li>
					          	<li>
						            <a href="">
						              <i class="fa fa-facebook icono-footer" ></i>
						                Facebook
						            </a>
					          	</li>
					         	<li>
						            <a href="">
						              <i class="fa fa-instagram icono-footer" aria-hidden="true"></i>
						                Instagram
						            </a>
					          	</li>
					          	<li>
						            <a href="">
						              <i class="fa fa-twitter icono-footer"></i>
						                Twitter
						            </a>
					          	</li>
							</ul>
						</aside>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div id="derechos_reservados" class="text-center"><span class="copyright" id="texto-footer">© 2018 ToFactory, Derechos reservados. Desarrollado por Axioma Developers</span></div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- Footer end-->

		<div class="scroll-top" onclick="subir_top()"><i class="fa fa-angle-up"></i></div>
	</div>
	<!-- Wrapper end-->
</div>
		<!-- Layout end-->

		<!-- Off canvas-->
		<div class="off-canvas-sidebar">
			<div class="off-canvas-sidebar-wrapper">
				<div class="off-canvas-header"><a class="close-offcanvas" href="index-21.html#"><span class="arrows arrows-arrows-remove"></span></a></div>
				<div class="off-canvas-content">
					<!-- Text widget-->
					<aside class="widget widget_text">
						<div class="textwidget">
							<p class="text-center"><img src="assets/images/logo-light.png" width="100" alt=""></p>
						</div>
					</aside>
					<!-- Text widget-->
					<aside class="widget widget_text">
						<div class="textwidget">
							<p class="text-center"><img src="assets/images/offcanvas.jpg" alt=""></p>
						</div>
					</aside>
					<!-- Navmenu widget-->
					<aside class="widget widget_nav_menu">
						<ul class="menu">
							<li class="menu-item menu-item-has-children"><a href="index-21.html#">Home</a></li>
							<li class="menu-item"><a href="index-21.html#">About Us</a></li>
							<li class="menu-item"><a href="index-21.html#">Services</a></li>
							<li class="menu-item"><a href="index-21.html#">Portfolio</a></li>
							<li class="menu-item"><a href="index-21.html#">Blog</a></li>
							<li class="menu-item"><a href="index-21.html#">Shortcodes</a></li>
						</ul>
					</aside>
					<ul class="social-icons">
						<li><a href="index-21.html#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-vk"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Off canvas end-->

		<!-- Scripts-->
		<script src="<?=base_url();?>assets/web/jquery-2.2.4.min.js"></script>
		<script src="<?=base_url();?>assets/web/popper.min.js"></script>
		<script src="<?=base_url();?>assets/web/bootstrap/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA0rANX07hh6ASNKdBr4mZH0KZSqbHYc3Q"></script>
		<script src="<?=base_url();?>assets/web/js/plugins.min.js"></script>
		<script src="<?=base_url();?>assets/web/js/charts.js"></script>
		<script src="<?=base_url();?>assets/web/js/custom.min.js"></script>
		<!--Plugins -->
		<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/wow/wow.min.js"></script>
		<script src="<?=base_url();?>assets/web/js/fbasic.js"></script>
		<script src="<?=base_url();?>assets/web/js/inicio.js"></script>
		<!-- -->
	</body>
</html>
