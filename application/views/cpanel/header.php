<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="@santu1987">
		<title>TuFactoring | Invoice Market</title>
		<!-- Favicons-->
		<link rel="shortcut icon" href="<?=base_url();?>assets/web/images/favicon.png">
		<link rel="apple-touch-icon" href="<?=base_url();?>assets/web/images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=base_url();?>assets/web/images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=base_url();?>assets/web/images/apple-touch-icon-114x114.png">
		<!-- Web Fonts-->
		<link href="<?=base_url();?>assets/web/css/fontsgoogleapis.css" rel="stylesheet">
		<!-- Bootstrap core CSS-->
		<link href="<?=base_url();?>assets/web/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Plugins and Icon Fonts-->
		<link href="<?=base_url();?>assets/web/css/plugins.min.css" rel="stylesheet">
		<!-- Template core CSS-->
		<link href="<?=base_url();?>assets/web/css/template.min.css" rel="stylesheet">
		<!-- Plugins -->
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.css">
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.css">
	    <link rel="stylesheet" href="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/web/css/index.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/web/css/zoom.css">
		<link rel="stylesheet" href="<?=base_url();?>assets/web/fonts/font-awesome/css/font-awesome.min.css" type="text/css"/>

	</head>
	<body>
