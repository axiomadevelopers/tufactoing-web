<!-- Wrapper-->
<div class="wrapper">
	<!-- Parallax de contactanos-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax4.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax4">CONTACTANOS</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- -->
	<section class="module">
		<div class="container">
			<div class="row m-b-50 wow fadeInDown">
					<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
						<section id="contact-names" class="">
		                    <div class="special-heading div-contact-names">
								<h4><span id="titulo-contacto2">Mantente en contacto</span></h4>
							</div>
		                    <div class="row text-center">
								<ul class="ul-contactos">
									<li id="subtitulo-contacto">A través de nuestro correo electrónico:</li>
									<li>
										<a href="mailto:support@tufactoring.com"><i class="fa fa-envelope-o icono-contactos" ></i> contact@tufactoring.com</a> <br/>
									</li>
									<li id="subtitulo-contacto2">Síguenos en las redes sociales:</li>
						          	<li>
							            <a href="">
							              <i class="fa fa-facebook icono-contactos" ></i>
							              Facebook
							            </a>
						          	</li>
						         	<li>
							            <a href="">
							              <i class="fa fa-instagram icono-contactos" aria-hidden="true"></i>
							              Instagram
							            </a>
						          	</li>
						          	<li>
							            <a href="">
							              <i class="fa fa-twitter icono-contactos"></i>
							              Twitter
							            </a>
						          	</li>
								</ul>
		                    </div>
		                </section>
					</div>
					<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 m-auto form-contact">
						<div class="special-heading">
							<h4><span id="titulo-contacto">Comunicate con nosotros</span></h4>
						</div>
						<input id="form-nombre" class="form-control form-control-lg m-b-10 campos-form" type="text" placeholder="NOMBRE">

						<input id="form-tlf" class="form-control form-control-lg m-b-10 campos-form" type="text" placeholder="TELÉFONO" onKeyPress="return valida(event,this,21,14)" onBlur="valida2(this,21,14);formato_tlf(this,'campo_mensaje_clientes');">

						<input id="form-email" class="form-control form-control-lg m-b-10 campos-form" type="text" placeholder="EMAIL" onBlur="valida2(this,5,50);correo(this,'campo_mensaje_clientes')">

						<textarea id="form-mensaje" class="form-control campos-form" id="exampleTextarea" rows="5" placeholder="MENSAJE"></textarea>
						
						<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mensaje-clientes" >
		                  	<div id="campo_mensaje_clientes" ></div>
		              	</div>
						<div class="col-md-8 m-auto">
							<div class="m-b-10">
								<div class="btn btn-block btn-round btn-lg btn-brand btn-enviar" onclick="insertarMensaje()">Enviar</div>
							</div>
						</div>
					</div>	
			</div>
		</div>
	</section>
	<!-- -->
	<!-- Parallax de multimedia-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax7.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax7">MULTIMEDIA</h1>
					</div>
				</div>
			</div>
		</div>

	</section>
	<section class="module" >
		<!-- -->
		<div id="contenedor_vi" name="contenedor_vi">  
			<div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
			    <div class="fadeInUp wow">
			      	<div class="col-lg-12" style="cursor: pointer" >
				      	<div id="contenedor_vi" name="contenedor_vi">
				       		<div class="content_video">
					          <div class="content_iframe_video centrado">
					            <iframe class="centrado" id="reproductor" width="100%" style="height: 400px" src="" frameborder="0" allowfullscreen></iframe>
					          </div>
					        </div>
					        <div style="clear: both;"></div>
				      	</div>
			        <!--<img src="site_media/img/img_web/videos2.png" class="super_imagen" >-->
			      	</div>
			      	<!--<div class="col-lg-12 video-muestra">
		              <h3 class="tituloVideo">Video de muestra</h3>
			      	</div>-->
			      	<div style="clear:both"></div>
			    </div>
			</div>
		</div>
	    <!-- -->
	</section>
	<!-- Parallax de redes sociales-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax8.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax8">REDES SOCIALES</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="redes_sociales">
        <div class="container">
            <div class="row pb-35">
                <p class="introduccion_parrafos" style="text-align: center">
                </p>
                <!-- fadeInUp wow -->
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding0">
                  <div style="" class="cuerpo_redes col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;">
                      <div class="redes_individual" style="display: flex;margin: 0 auto">
                        <a href="https://www.facebook.com/Tu-Factoring-2305965059619696/?modal=admin_todo_tour" target="_blank">
                            <div class="contenedor_icono_pasos">
                              <div class="iconos_pasos iconos_redes">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                              </div>
                            </div>
                        </a>    
                      </div>
                      <div style="clear:both"></div>
                    </div>
                    <div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;" title="Facebook">
                      <div  class="redes_individual" style="display: flex;margin: 0 auto">
                        <a href="" target="_blank">
                            <div class="contenedor_icono_pasos">
                              <div class="iconos_pasos iconos_redes">
                                  <i class="fa fa-instagram" aria-hidden="true"></i>
                              </div>
                            </div>
                        </a>    
                      </div>
                      <div style="clear:both"></div>
                    </div>
                    <div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual"  style="float:left;display: flex; margin: 0 auto;">
                      <div class="redes_individual" style="float:left;display: flex; margin: 0 auto;">
                        <a href="https://twitter.com/FactoringTu" target="_blank">
                            <div class="contenedor_icono_pasos">
                              <div class="iconos_pasos iconos_redes" href="" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                              </div>
                            </div>
                        </a>    
                      </div>
                      <div style="clear:both"></div>
                    </div>

                    <div style="clear:both"></div>
                  </div>
                </div>

                <!-- Bloque de facebook -->
                <!-- 0 -->
                <!--<div class="col-lg-4 fadeInLeft wow redes_cuadro hidden-0 hidden-md hidden-sm  tamano_facebook" id="row_redes" style="float:left">
                    <div class=" div_facebook ">
                       <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Faktivedigital&tabs=timeline&width=325&height=505&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="325" height="505" style="border:none;overflow:hidden;margin: 0 auto;display: flex;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                    </div>
                </div>-->
                <!-- fadeInLeft wow  -->
                <div class="super_contenedor_redes">
	                <div class="row" style="padding-bottom: 35px;">
	                    <div class="fadeIn wow col-lg-4 redes_cuadro hidden-md hidden-sm hidden-xs centrado" id="row_redes">
	                        <div class=" div_facebook ">
	                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="350" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" class="centrado"></iframe>  
	                        </div>
	                    </div>  
	                    <!-- fadeInLeft wow -->
	                    <div class="fadeIn wow col-md-4  redes_cuadro hidden-lg hidden-sm hidden-xs centrado" id="row_redes" style="float:left">
	                        <div class="div_facebook ">
	                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=200&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="200" height="500" style="border:none;overflow:hidden;float:left" scrolling="no" frameborder="0" allowTransparency="true"></iframe> 
	                        </div>
	                    </div>
	                    <!-- fadeInLeft  wow  -->
	                    <div class="fadeIn wow col-sm-12 redes_cuadro hidden-lg hidden-md hidden-xs centrado" id="row_redes" style="display: flex">
	                       <div class="div_facebook">
	                         <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500" height="500" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowTransparency="true" class=""></iframe>  
	                      </div>
	                    </div>
	                    <!-- fadeInLeft wow  -->
	                    <div class="fadeIn wow col-xs-12 redes_cuadro hidden-lg hidden-md hidden-sm centrado" id="row_redes">
	                       <div class="div_facebook">
	                         <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" class="centrado"></iframe>  
	                      </div>
	                    </div> 
	                    <!-- fadeInUp wow  -->
	                	<div class="fadeIn wow col-lg-4 col-md-4 col-sm-12 col-xs-12 redes_cuadro" style="display: flex">
		                    <div class=" div_twitter tamano_twitter centrado" style="max-height: 500px;overflow-x: hidden">
		                        <!--<script src="//www.powr.io/powr.js" external-type="html"></script>
		                        <div class="powr-instagram-feed" id="edcb5215_1506621190"></div>-->
		                        <!-- LightWidget WIDGET -->
		                        <script src="//lightwidget.com/widgets/lightwidget.js"></script>
		                        <div class="tamano_twitter" style="max-height: 510px;overflow-x: hidden;">
		                          <!-- <iframe src="//lightwidget.com/widgets/8072b9eabfa75f5cac29636996153236.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe> -->
		                          <!-- -->
		                          <!-- LightWidget WIDGET -->
		                          <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/8072b9eabfa75f5cac29636996153236.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;" ></iframe>
		                        </div>
		                    </div>
		                </div>
	                    <!-- fadeInRight wow -->
	                    <div class="fadeIn wow col-lg-4 col-md-4 col-sm-12 col-xs-12  redes_cuadro " style="float:left;">
	                        <!-- fadeInLeft wow  -->    
							<div class="redes_cuadro centrado" id="">
								<div class=" div_twitter tamano_twitter centrado" style="max-height: 500px;overflow-x: hidden">
								   <a class="twitter-timeline" href="https://twitter.com/FactoringTu">Tweets by @FactoringTu</a>
								   <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								</div>
							</div>
	                    </div>
                    </div>
            	</div>
            	<div style="clear:both"></div>
            </div>
        </div>    
    </section>