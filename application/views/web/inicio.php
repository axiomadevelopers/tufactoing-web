<!-- Wrapper-->
<div class="wrapper">
	
	<!-- Page Header-->
	<section class="module-slides">
		<ul class="slides-container">

			<li class="bg-dark bg-dark-30"><img src="<?=base_url();?>assets/web/images/slider/slider1.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6" style="text-align: center">
									<div class="col-lg-12">
										<h1 class="h1 m-b-20 wow fadeInDown titulo_tufactoring" id="titulo_tufactoring" data-wow-delay="0.5s" style="" >Tu Factoring  </h1>
									</div>
									<!-- Solo para moviles...-->
									<div class="col-lg-12 mobile-title">
										<div class="m-b-40 wow fadeInDown" data-wow-delay="0.7s">
											
											<span class="subtitulo_slide titulo_slide1">
												
											</span>
											|
											<span class="subtitulo_tufactoring titulo_tofactoring">
												
											</span> 
										</div>
									</div>	
									<!-- -->
									<div class="col-lg-12">
										<p class="m-b-40 wow fadeInDown subtitulo_slide titulo_slide1 subtitulo_slide_grande" data-wow-delay="0.7s" style="" >Mercado de Facturas</p>
									</div>
								</div>
								<h4 style="" class="separador">|</h4>
								<div class="col-md-4 bloque-subtitulo-tu-factoring" style="text-align: left">		
									<h3 class="h1 m-t-20 wow fadeInDown subtitulo_tufactoring titulo_tofactoring" data-wow-delay="0.5s" id="titulo_tofactoring"> It's Time to Win </h3>
								</div>
							</div>		
							<!--<p><a class="btn btn-circle btn-outline btn-lg btn-white wow fadeInDown" data-wow-delay="0.9s" href="index-21.html#" id="btn_slide1">Comenzar</a></p> -->
						</div>
					</div>
				</div>
			</li>

			<li class="bg-dark bg-dark-30"><img src="<?=base_url();?>assets/web/images/slider/slider2.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="h1 m-b-20 wow fadeInDown" data-wow-delay="0.5s" id="titulo_ppal_slide2">El apoyo ideal para el crecimiento de su negocio</h1>
							<p class="m-b-40 wow fadeInDown" data-wow-delay="0.7s" id="titulo_slide2">Comience a trabajar con nosotros.</p>
							<!--<p><a class="btn btn-circle btn-lg btn-primary wow fadeInDown" data-wow-delay="0.9s" href="index-21.html#" id="btn_slide2">Quienes somos</a></p>-->
						</div>
					</div>
				</div>
			</li>

			<li class="bg-dark bg-gradient"><img src="<?=base_url();?>assets/web/images/slider/slider3.jpg" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="h1 m-b-20 wow fadeInDown" data-wow-delay="0.5s" id="titulo_ppal_slide3">El mejor aliado</h1>
							<p class="m-b-40 wow fadeInDown" data-wow-delay="0.7s" id="titulo_slide3">Para su negocio en crecimiento</p>
							<!--<p><a class="btn btn-circle btn-lg btn-brand wow fadeInDown " id="btn_slide3" data-wow-delay="0.9s" href="index-21.html#">Ir a servicios</a></p> -->
						</div>
					</div>
				</div>
			</li>
			
			
		</ul>
		<nav class="slides-navigation"><a class="next" href="index-21.html#"><i class="fa fa-angle-right" aria-hidden="true"></i></a><a class="prev" href="index-21.html#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></nav>
	</section>
	<!-- Page Header end-->
	
	<!-- Quienes somos -->
	<section class="module">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
					<div class="module-title text-center wow fadeInUp">
						<h2>TuFactoring</h2>
						<p class="font-serif" id="subtitulo-qsomos" >Mercado de facturas</p>
					</div>
					<div class="wow fadeInUp">
						<p class="texto-parrafos" id="parrafo-qsomos-home">
							 Es un novedoso lugar de encuentro electrónico que combina la tecnología con las operaciones de Confirming (Pago Proveedores), Factoring y la compra  de facturas junto a la incorporación de  un proceso de subasta. Las empresas proveedores de productos y servicios podrán financiar su capital de trabajo  a tasas muy competitivas, los inversionistas encontrarán un instrumento de inversión con tasas muy atractivas y las empresas demandantes de productos y servicios podrán realizar el pago de sus cuentas por pagar a través de nuestros bancos aliados 
						</p>
					</div>
				</div>
				<!--centrador-->
				<div id="centrador" class="fadeInDown wow row">
					<!--card izquierda-->
					<div id="izquierda" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
						<center>
							<div class="icon-box-icon">
								<span class="fa-stack fa-lg">
			                      	<i class="fa fa-circle fa-stack-2x"></i>
								 	<i class="fa fa-lock fa-stack-1x fa-inverse" aria-hidden="true"></i>
			                    </span>
								<div style="clear: both"></div>
							</div>
								
							<div class="icon-box-title">
								<h6 id="titulo_clientes1" >Seguridad en tu inversión  (INVERSIONISTA)</h6>
							</div>
							
							<div class="icon-box-content hide" id="icon-box-content-iz">
								<p id="parrafos_clientes1">Altos rendimientos en tu inversión con bajo riesgo ya que contarás con la confirmación de pago por parte de un banco aliado al vencimiento de la factura</p>
							</div>
						</center>
					</div>
					<!--card izquierda end-->
					<!--card centro-->
					<div id="centro" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
						<center>
							<div class="icon-box-icon">
								<span class="fa-stack fa-lg">
			                      	<i class="fa fa-circle fa-stack-2x"></i>
								 	<i class="fa fa-suitcase fa-stack-1x fa-inverse" aria-hidden="true"></i>
			                    </span>
								<div style="clear: both"></div>
							</div>	
							<div class="icon-box-title">
								<h6 id="titulo_clientes2">Extraordinarios beneficios (PROVEEDOR)</h6>
							</div>
								
							<div class="icon-box-content hide" id="icon-box-content-cen">
								<p id="parrafos_clientes2">Procesos agiles para la venta de tus facturas sin necesidad de acudir a una institución financiera, en subasta, aprobarás la tasa de descuento ofertada más conveniente para tu empresa, proveerte de capital de trabajo para potenciar tus nuevas ventas</p>
							</div>
						</center>
					</div>
					<!--card centro end-->
					<!--card derecha -->
					<div id="derecha" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
						<center>
							<div class="icon-box-icon">
								<span class="fa-stack fa-lg">
			                      	<i class="fa fa-circle fa-stack-2x"></i>
								 	<i class="fa fa-university fa-stack-1x fa-inverse" aria-hidden="true"></i>
			                    </span>
								<div style="clear: both"></div>
							</div>	
							<div class="icon-box-title">
								<h6 id="titulo_clientes3">Gestión de cuentas por pagar</h6>
							</div>
							
							<div class="icon-box-content hide" id="icon-box-content-de">
								<p id="parrafos_clientes3">Como cliente de nuestros Bancos Aliados, tendrás acceso a servicios y productos financieros para la cancelación de tus cuentas por pagar.</p>
							</div>
						</center>
					</div>
					<!--card derecha end-->
					<div style="clear: both"></div>
				</div>
				<div class="row centrado boton-nosotros" >
					<div class="col-md-12">
						<div class="text-center"><a class="btn btn-lg btn-circle btn-shadow btn-white btn-ver-mas leer-mas menu1" ></a></div>
					</div>
					<div style="clear: both;"></div>
				</div>
			<!--centrador end-->					
			</div>
		</div>	
	</section>
	<!--
	<div class="alert alert-primary alert-float" role="alert">
	  <button type="button" class="close" data-dismiss="alert">&times;</button>
	  Este es un ejemplo de alert realizado con bootstrap4 
	</div>
	-->
	<!-- Parallax de servicios-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax2.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax2">SERVICIOS</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Servicios fotos -->
	<section class="module">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto bloque-inicial">
					<div class="wow fadeInUp">
						<p class="texto-parrafos" id="parrafo-servicios-home">
							
						</p>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6 wow fadeInRight">
					<div class="feature ">
						<div class="feature-thumb"><img style="" src="<?=base_url();?>assets/web/images/features/1.jpg" alt="" class="img-servicios"></div>
						<div class="feature-content">
							<h6 id="titulo-comof1" class="titulos-fact">Gestión de Cuentas de Por Pagar Proveedores</h6>
							<p id="parrafo-comof1-home">Si eres cliente de nuestros Bancos Aliados, a través del sistema de Tufactoring podrás canalizar el pago de tus obligaciones con tus proveedores por medio de la aceptación de un servicio de Confirming con o sin financiamiento.</p>
						</div>
						<div class="feature-more"><a class="a-menu2 leer-mas">Read More →</a></div>						
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6 wow fadeInUp">
					<div class="feature">
						<div class="feature-thumb"><img src="<?=base_url();?>assets/web/images/features/2.jpg" alt="" class="img-servicios"></div>
						<div class="feature-content">
							<h6 id="titulo-comof2" class="titulos-fact">Negociación</h6>
							<p id="parrafo-comof2-home">el proceso de subasta te permitirá realizar compras de facturas sin límite de inversión, con la confianza de pago por la existencia de un banco aliado que te confirmará que pagará al vencimiento de la factura. Al cierre de la jornada de la subasta, el ganador será el que mejor oferta de descuenta presente al vendedor. </p>
						</div>
						<div class="feature-more"><a class="a-menu2 leer-mas">Read More →</a></div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-6 wow fadeInLeft">
					<div class="feature">
						<div class="feature-thumb"><img src="<?=base_url();?>assets/web/images/features/3.jpg" alt="" class="img-servicios"></div>
						<div class="feature-content">
							<h6 id="titulo-comof3" class="titulos-fact">Inversores</h6>
							<p id="parrafo-comof3-home">Como cliente de nuestros Bancos Aliados, tendrás acceso a servicios y productos financieros para la cancelación de tus cuentas por pagar.</p>
						</div>
						<div class="feature-more"><a class="a-menu2 leer-mas">Read More →</a></div>
					</div>
				</div>

			</div>
			<!--<div class="row m-t-80">
				<div class="col-md-12">
					<div class="text-center"><a class="btn btn-lg btn-circle btn-shadow btn-white btn-ver-mas ver-mas menu2" href="" ></a></div>
				</div>
			</div>-->
		</div>
	</section>
	<!-- Como funciona end-->
	<!-- -->
	

	<!-- Parallax de aliados-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax5.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax3">BANCOS ALIADOS</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- BANCOS -->
	<section class="module-sm module-gray aliados">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel clients-carousel" data-carousel-options="{&quot;items&quot;:&quot;4&quot;}">
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-1.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-2.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-7.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-6.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-5.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Parallax de aliados-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax3.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax5">CLIENTES</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--  -->
	<section class="module-sm module-gray aliados">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel clients-carousel" data-carousel-options="{&quot;items&quot;:&quot;4&quot;}">
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-1.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-2.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-7.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-6.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-5.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Parallax de noticias-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax6.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax6">NOTICIAS</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Noticias -->    
	<section class="module" >
		<div class="container">
		    <div class="row">
		        <div class="col-md-6 col-lg-4 col-sm-12 col-xs-12  wow fadeInLeft">
					<!-- Post-->
					<article class="post">
						<div class="post-background" data-background="<?=base_url();?>assets/web/images/news/news1.jpg">
							<div class="post-header">
								<h2 class="post-title-home">
									<label id="titulo_not_1"></label>
								</h2>
								<ul class="post-meta-home h5">
									<li>
										<i class="fa fa-calendar" aria-hidden="true"></i>
		                      			<span id="fecha_not_1" ></span>
									</li>
									<li>
										<i class="fa fa-user" aria-hidden="true"></i>
		                      			<label id="usuario_not_1" ></label>
									</li>
								</ul>
							</div><a class="post-background-link slug_not_1" id=""></a>
						</div>
					</article>
					<!-- Post end-->
		            <!-- /.blog-preview-content -->
		        </div><!-- /.blog-col -->
		        <div class="col-md-6 col-lg-4  col-sm-12 col-xs-12 wow fadeInUp">
		        	<!-- Post-->
					<article class="post">
						<div class="post-background" data-background="<?=base_url();?>assets/web/images/news/news2.jpg">
							<div class="post-header">
								<h2 class="post-title-home">
									<label id="titulo_not_2"></label>
								</h2>
								<ul class="post-meta-home h5">
									<li>
										<i class="fa fa-calendar" aria-hidden="true"></i>
		                      			<span id="fecha_not_2" ></span>
									</li>
									<li>
										<i class="fa fa-user" aria-hidden="true"></i>
		                      			<label id="usuario_not_2" ></label>
									</li>
								</ul>
							</div><a class="post-background-link slug_not_2" id=""></a>
						</div>
					</article>
					<!-- Post end-->
		        </div><!-- /.blog-col -->
		        <div class="col-md-6 col-lg-4  col-sm-12 col-xs-12 wow fadeInRight">
		        	<!-- Post-->
					<article class="post">
						<div class="post-background" data-background="<?=base_url();?>assets/web/images/news/news3.jpg">
							<div class="post-header">
								<h2 class="post-title-home">
									<label id="titulo_not_3"></label>
								</h2>
								<ul class="post-meta-home h5">
									<li>
										<i class="fa fa-calendar" aria-hidden="true"></i>
		                      			<span id="fecha_not_3" ></span>
									</li>
									<li>
										<i class="fa fa-user" aria-hidden="true"></i>
		                      			<label id="usuario_not_3" ></label>
									</li>
								</ul>
							</div><a class="post-background-link slug_not_3" id=""></a>
						</div>
					</article>
					<!-- Post end-->
		        </div><!-- /.blog-col -->

		    </div><!-- /.blog-row -->
		    <div class="row m-t-80">
				<div class="col-md-12">
					<div class="text-center"><a class="btn btn-lg btn-circle btn-shadow btn-white btn-ver-mas ver-mas menu4" >Ver mas</a></div>
				</div>
			</div>
		</div><!-- /.container -->
	</section>
	<!-- Parallax de multimedia-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax7.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax7">MULTIMEDIA</h1>
					</div>
				</div>
			</div>
		</div>

	</section>
	<section class="module" >
		<!-- -->
		<div id="contenedor_vi" name="contenedor_vi">  
			<div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
			    <div class="fadeInUp wow">
			      	<div class="col-lg-12" style="cursor: pointer" >
				      	<div id="contenedor_vi" name="contenedor_vi">
				       		<div class="content_video">
					          <div class="content_iframe_video centrado">
					            <iframe class="centrado" id="reproductor" width="100%" style="height: 400px" src="" frameborder="0" allowfullscreen></iframe>
					          </div>
					        </div>
					        <div style="clear: both;"></div>
				      	</div>
			        <!--<img src="site_media/img/img_web/videos2.png" class="super_imagen" >-->
			      	</div>
			      	<!--<div class="col-lg-12 video-muestra">
		              <h3 class="tituloVideo">Video de muestra</h3>
			      	</div>-->
			      	<div style="clear:both"></div>
			    </div>
			</div>
		</div>
	    <!-- -->
	</section>
	<!-- Parallax de redes sociales-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax8.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax8">REDES SOCIALES</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="redes_sociales">
        <div class="container">
            <div class="row pb-35">
                <p class="introduccion_parrafos" style="text-align: center">
                </p>
                <!-- fadeInUp wow -->
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding0">
                  <div style="" class="cuerpo_redes col-lg-12 col-md-12 col-xs-12 col-sm-12">
                    <div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;">
                      <div class="redes_individual" style="display: flex;margin: 0 auto">
                        <a href="https://www.facebook.com/Tu-Factoring-2305965059619696/?modal=admin_todo_tour" target="_blank">
                            <div class="contenedor_icono_pasos">
                              <div class="iconos_pasos iconos_redes">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                              </div>
                            </div>
                        </a>    
                      </div>
                      <div style="clear:both"></div>
                    </div>
                    <div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual" style="float:left;display: flex; margin: 0 auto;" title="Facebook">
                      <div  class="redes_individual" style="display: flex;margin: 0 auto">
                        <a href="" target="_blank">
                            <div class="contenedor_icono_pasos">
                              <div class="iconos_pasos iconos_redes">
                                  <i class="fa fa-instagram" aria-hidden="true"></i>
                              </div>
                            </div>
                        </a>    
                      </div>
                      <div style="clear:both"></div>
                    </div>
                    <div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual"  style="float:left;display: flex; margin: 0 auto;">
                      <div class="redes_individual" style="float:left;display: flex; margin: 0 auto;">
                        <a href="https://twitter.com/FactoringTu" target="_blank">
                            <div class="contenedor_icono_pasos">
                              <div class="iconos_pasos iconos_redes" href="" target="_blank">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                              </div>
                            </div>
                        </a>    
                      </div>
                      <div style="clear:both"></div>
                    </div>

                    <div style="clear:both"></div>
                  </div>
                </div>

                <!-- Bloque de facebook -->
                <!-- 0 -->
                <!--<div class="col-lg-4 fadeInLeft wow redes_cuadro hidden-0 hidden-md hidden-sm  tamano_facebook" id="row_redes" style="float:left">
                    <div class=" div_facebook ">
                       <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Faktivedigital&tabs=timeline&width=325&height=505&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="325" height="505" style="border:none;overflow:hidden;margin: 0 auto;display: flex;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                    </div>
                </div>-->
                <!-- fadeInLeft wow  -->
                <div class="super_contenedor_redes">
	                <div class="row" style="padding-bottom: 35px;">
	                    <div class="fadeIn wow col-lg-4 redes_cuadro hidden-md hidden-sm hidden-xs centrado" id="row_redes">
	                        <div class=" div_facebook ">
	                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="350" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" class="centrado"></iframe>  
	                        </div>
	                    </div>  
	                    <!-- fadeInLeft wow -->
	                    <div class="fadeIn wow col-md-4  redes_cuadro hidden-lg hidden-sm hidden-xs centrado" id="row_redes" style="float:left">
	                        <div class="div_facebook ">
	                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=200&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="200" height="500" style="border:none;overflow:hidden;float:left" scrolling="no" frameborder="0" allowTransparency="true"></iframe> 
	                        </div>
	                    </div>
	                    <!-- fadeInLeft  wow  -->
	                    <div class="fadeIn wow col-sm-12 redes_cuadro hidden-lg hidden-md hidden-xs centrado" id="row_redes" style="display: flex">
	                       <div class="div_facebook">
	                         <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500" height="500" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowTransparency="true" class=""></iframe>  
	                      </div>
	                    </div>
	                    <!-- fadeInLeft wow  -->
	                    <div class="fadeIn wow col-xs-12 redes_cuadro hidden-lg hidden-md hidden-sm centrado" id="row_redes">
	                       <div class="div_facebook">
	                         <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FTu-Factoring-2305965059619696%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" class="centrado"></iframe>  
	                      </div>
	                    </div> 
	                    <!-- fadeInUp wow  -->
	                	<div class="fadeIn wow col-lg-4 col-md-4 col-sm-12 col-xs-12 redes_cuadro" style="display: flex">
		                    <div class=" div_twitter tamano_twitter centrado" style="max-height: 500px;overflow-x: hidden">
		                        <!--<script src="//www.powr.io/powr.js" external-type="html"></script>
		                        <div class="powr-instagram-feed" id="edcb5215_1506621190"></div>-->
		                        <!-- LightWidget WIDGET -->
		                        <script src="//lightwidget.com/widgets/lightwidget.js"></script>
		                        <div class="tamano_twitter" style="max-height: 510px;overflow-x: hidden;">
		                          <!-- <iframe src="//lightwidget.com/widgets/8072b9eabfa75f5cac29636996153236.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe> -->
		                          <!-- -->
		                          <!-- LightWidget WIDGET -->
		                          <script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/8072b9eabfa75f5cac29636996153236.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;" ></iframe>
		                        </div>
		                    </div>
		                </div>
	                    <!-- fadeInRight wow -->
	                    <div class="fadeIn wow col-lg-4 col-md-4 col-sm-12 col-xs-12  redes_cuadro " style="float:left;">
	                        <!-- fadeInLeft wow  -->    
							<div class="redes_cuadro centrado" id="">
								<div class=" div_twitter tamano_twitter centrado" style="max-height: 500px;overflow-x: hidden">
								   <a class="twitter-timeline" href="https://twitter.com/FactoringTu">Tweets by @FactoringTu</a>
								   <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								</div>
							</div>
	                    </div>
                    </div>
            	</div>
            	<div style="clear:both"></div>
            </div>
        </div>    
    </section>	
	<!-- Parallax de contactanos-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax4.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax4">CONTACTANOS</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- -->
	<section class="module">
		<div class="container">
			<div class="row m-b-50 wow fadeInDown">
					<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
						<section id="contact-names" class="">
		                    <div class="special-heading div-contact-names">
								<h4><span id="titulo-contacto2">Mantente en contacto</span></h4>
							</div>
		                    <div class="row text-center">
								<ul class="ul-contactos">
									<li id="subtitulo-contacto">A través de nuestro correo electrónico:</li>
									<li>
										<a href="mailto:support@tufactoring.com"><i class="fa fa-envelope-o icono-contactos" ></i> contact@tufactoring.com</a> <br/>
									</li>
									<li id="subtitulo-contacto2">Síguenos en las redes sociales:</li>
						          	<li>
							            <a href="">
							              <i class="fa fa-facebook icono-contactos" ></i>
							              Facebook
							            </a>
						          	</li>
						         	<li>
							            <a href="">
							              <i class="fa fa-instagram icono-contactos" aria-hidden="true"></i>
							              Instagram
							            </a>
						          	</li>
						          	<li>
							            <a href="">
							              <i class="fa fa-twitter icono-contactos"></i>
							              Twitter
							            </a>
						          	</li>
								</ul>
		                    </div>
		                </section>
					</div>
					<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 m-auto form-contact">
						<div class="special-heading">
							<h4><span id="titulo-contacto">Comunicate con nosotros</span></h4>
						</div>
						<input id="form-nombre" class="form-control form-control-lg m-b-10 campos-form" type="text" placeholder="NOMBRE">

						<input id="form-tlf" class="form-control form-control-lg m-b-10 campos-form" type="text" placeholder="TELÉFONO" onKeyPress="return valida(event,this,21,14)" onBlur="valida2(this,21,14);formato_tlf(this,'campo_mensaje_clientes');">

						<input id="form-email" class="form-control form-control-lg m-b-10 campos-form" type="text" placeholder="EMAIL" onBlur="valida2(this,5,50);correo(this,'campo_mensaje_clientes')">

						<textarea id="form-mensaje" class="form-control campos-form" id="exampleTextarea" rows="5" placeholder="MENSAJE"></textarea>
						
						<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mensaje-clientes" >
		                  	<div id="campo_mensaje_clientes" ></div>
		              	</div>
						<div class="col-md-8 m-auto">
							<div class="m-b-10">
								<div class="btn btn-block btn-round btn-lg btn-brand btn-enviar" onclick="insertarMensaje()">Enviar</div>
							</div>
						</div>
					</div>	
			</div>
		</div>
	</section>

				
