<!-- Wrapper-->
<div class="wrapper">
	<!-- Parallax de servicios-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax6.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax6">Noticias</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- fin de parallax -->
	<!-- inicio de section modules noticias -->
	<!-- Blog-->
	<section class="module">
		<div class="container">
			<div class="row">
				<!-- Content-->
				<div class="col-lg-8">
					<!-- Post-->
					<article class="post">
						<div class="post-preview fadeInLeft wow">
							<a href="blog.html#">
								<img id="img_not_1" alt="">
							</a>
						</div>
						<div class="post-wrapper">
							<div class="post-header fadeInLeft wow">
								<h2 class="post-title" id="titulo_not_1"><a href="blog-single.html"></a></h2>
								<ul class="post-meta h5">
									<div>
										<label id="fecha_not_1" ></label> | 
										<label id="usuario_not_1" ></label>
									</div>
								</ul>
							</div>
							<div class="post-content fadeInLeft wow">
								<p id="des_not_1"></p>
							</div>
							<div class="post-more fadeInUp wow"><a id="" href="" class=" slug_not_1 leer-mas">Read more →</a></div>
						</div>
					</article>
					<!-- Post end-->
				</div>		
				<!-- Sidebar-->
				<div class="col-lg-4 servicios-noticias">
					<div class="sidebar">
						<!-- Recent entries widget-->
						<aside class="widget widget_recent_entries_custom">
							<div class="widget-title fadeInDown wow">
								<h3 class="cta-servicios"></h3>
							</div>
							<ul>
								<li class="clearfix fadeInUp wow">
									<div class="wi"><img src="<?=base_url();?>assets/web/images/features/1.jpg" alt=""></div>
									<div class="wb"><a class="menu2" id="titulo-comof1"></a></div>
								</li>
								<li class="clearfix fadeInUp wow">
									<div class="wi"><img src="<?=base_url();?>assets/web/images/features/2.jpg" alt=""></div>
									<div class="wb"><a class="menu2" id="titulo-comof2"></a></div>
								</li>
								<li class="clearfix fadeInUp wow">
									<div class="wi"><img src="<?=base_url();?>assets/web/images/features/3.jpg" alt=""></div>
									<div class="wb"><a class="menu2" id="titulo-comof3"></a></div>
								</li>
							</ul>
						</aside>
					</div>
				</div>
				<!-- Sidebar end-->
				<hr>
				<div class="col-lg-12 widget-title fadeInUp wow">
					<hr></hr>
					<h3 id="otras-noticias"></h3>
				</div>
				<!-- Content -->
				<div class="col-lg-12">
					<div class="row blog-grid">
						<div class="col-md-6 col-lg-4 post-item">
							<!-- Post-->
							<article class="post fadeInLeft wow">
								<div class="post-preview">
									<a href="blog-grid-2.html#">
										<img id="img_not_2" class="img-news" alt="">
									</a>
								</div>
								<div class="post-wrapper">
									<div class="post-header ">
										<!--titulo noticia 2-->
										<h2 class="post-title" id="titulo_not_2"><a href="blog-single.html"></a></h2>
										<div class="post-meta h5">
											<!--fecha noticia 2-->
											<label id="fecha_not_2"></label> |
											<label id="usuario_not_2" ></label>
										</div>
									</div>
									<!--descripcion corta noticia 2-->
									<div class="post-content " >
										<p id="des_cor_not_2"></p>
									</div>
									<div class="post-more fadeInLeft wow"><a class="leer-mas slug_not_2" id="" href=""></a></div>
								</div>
							</article>
							<!-- Post end-->
						</div>
						<div class="col-md-6 col-lg-4 post-item">
							<!-- Post-->
							<article class="post fadeInUp wow">
								<div class="post-preview"><a href="blog-grid-2.html#"><img class="img-news"  src="<?=base_url();?>assets/web/images/news/news3.jpg" alt=""></a></div>
								<div class="post-wrapper">
									<div class="post-header">
									<!--titulo noticia 3-->
										<h2 class="post-title"><a href="blog-single.html" id="titulo_not_3" ></a></h2>
										<div class="post-meta h5">
											<!--fecha noticia 2-->
											<label id="fecha_not_3"></label> |
											<label id="usuario_not_3" ></label>
										</div>
									</div>
									<div class="post-content">
									<!--descripcion corta noticia 3-->
										<p id="des_cor_not_3"></p>
									</div>
									<div class="post-more"><a class="leer-mas slug_not_3" id="" href=""></a></div>
								</div>
							</article>
							<!-- Post end-->
						</div>
						<div class="col-md-6 col-lg-4 post-item">
							<!-- Post-->
							<article class="post fadeInRight wow">
								<div class="post-preview"><a href="blog-grid-2.html#"><img class="img-news" src="<?=base_url();?>assets/web/images/news/news4.jpg" alt=""></a></div>
								<div class="post-wrapper">
									<div class="post-header">
										<!--titulo noticia 4-->
										<h2 class="post-title" id="titulo_not_4" ><a href="blog-single.html"></a></h2>
										<div class="post-meta h5">
											<!--fecha noticia 2-->
											<label id="fecha_not_4"></label> |
											<label id="usuario_not_4" ></label>
										</div>
									</div>
									<div class="post-content">
										<p>Ut pharetra nibh facilisis sed. Duis cursus in eros hendrerit consequat. Aenean porttitor turpis in turpis tincidunt.</p>
									</div>
									<div class="post-more"><a class="leer-mas slug_not_4" id="" href=""></a></div>
								</div>
							</article>
							<!-- Post end-->
						</div>
					</div>
				</div>
				<!-- Content end -->
			</div>
		</div>
	</section>
	<!-- Blog end-->
	<!-- fin de section modeules noticias-->
