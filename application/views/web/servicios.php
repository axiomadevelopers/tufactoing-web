<!-- Wrapper-->
<div class="wrapper">
	<!-- Parallax de servicios-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax2.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax2">SERVICIOS</h1>
					</div>
				</div>
			</div>	
	</section>
	<!-- -->
	<section class="module">
		<div class="container">
			<div class="row p-bt-35">
				<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 m-auto bloque-inicial">
					<div class="wow fadeInUp">
						<p class="texto-parrafos" id="parrafo-servicios">
							
						</p>
					</div>
				</div>
			</div>	
			
			<div class="row p-bt-35">
				<div class="col-lg-8">
					<!-- Post-->
					<article class="post fadeInLeft wow">
						<div class="post-header">
							<h1 class="post-title" id="titulo-comof1-serv"></h1>
						</div>
						<div class="post-preview"><img  src="<?=base_url();?>assets/web/images/features/1.jpg" alt=""></div>
						<div class="post-content">
							<div id="parrafo-comof1-serv"></div>
						</div>
					</article>
				</div>
				<div class="col-lg-4">
					<aside class="widget widget_recent_entries_custom fadeInRight wow">
						<div class="widget-title">
							<h5 class="titulo_como_funciona">COMO FUNCIONA</h5>
						</div>
						<div class="post-content">
							<div id="parrafo-comof1-serv-gen"></div>
						</div>
					</aside>	
				</div>
			</div>
			<hr>
			<div class="row p-bt-35">
				<div class="col-lg-4 como_funciona_ver">
					<aside class="widget widget_recent_entries_custom fadeInLeft wow">
						<div class="widget-title">
							<h5 class="titulo_como_funciona">COMO FUNCIONA</h5>
						</div>
						<div class="post-content">
							<div class="parrafo-comof2-serv-gen"></div>
						</div>
					</aside>	
				</div>
				<div class="col-lg-8">
					<!-- Post-->
					<article class="post fadeInRight wow">
						<div class="post-header">
							<h1 class="post-title" id="titulo-comof2-serv"></h1>
						</div>
						<div class="post-preview"><img  src="<?=base_url();?>assets/web/images/features/2.jpg" alt=""></div>
						<div class="post-content">
							<div id="parrafo-comof2-serv"></div>
						</div>
					</article>
				</div>
				<div class="col-lg-4 como_funciona_ver_peque">
					<aside class="widget widget_recent_entries_custom fadeInLeft wow ">
						<div class="widget-title">
							<h5 class="titulo_como_funciona">COMO FUNCIONA</h5>
						</div>
						<div class="post-content">
							<div class="parrafo-comof2-serv-gen"></div>
						</div>
					</aside>	
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-8">
					<!-- Post-->
					<article class="post fadeInLeft wow">
						<div class="post-header">
							<h1 class="post-title" id="titulo-comof3-serv"></h1>
						</div>
						<div class="post-preview"><img  src="<?=base_url();?>assets/web/images/features/3.jpg" alt=""></div>
						<div class="post-content">
							<div id="parrafo-comof3-serv"></div>
						</div>
					</article>
				</div>
				<div class="col-lg-4">
					<aside class="widget widget_recent_entries_custom fadeInRight wow">
						<div class="widget-title">
							<h5 class="titulo_como_funciona">COMO FUNCIONA</h5>
						</div>
						<div class="post-content">
							<div id="parrafo-comof3-serv-gen"></div>
						</div>
					</aside>	
				</div>
			</div>
			<div class="row m-t-80">
				<div class="col-md-12">
					<div class="text-center"><a class="btn btn-lg btn-circle btn-shadow btn-white cta-contactanos menu3">Contactanos</a></div>
				</div>
			</div>
		</div>	
	</section>	