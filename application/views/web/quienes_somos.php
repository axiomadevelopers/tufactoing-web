<!-- Wrapper-->
<div class="wrapper">

	
	<!-- Parallax de quienes somos-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax9.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax9"></h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- -->
	<!-- Quienes somos -->
	<section class="module">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
					<div class="module-title text-center wow fadeInUp">
						<h2>TuFactoring</h2>
						<p class="font-serif" id="subtitulo-qsomos" >Mercado de facturas</p>
					</div>
					<div class="wow fadeInUp">
						<p class="texto-parrafos" id="parrafo-qsomos">
							 Es un novedoso lugar de encuentro electrónico que combina la tecnología con las operaciones de Confirming (Pago Proveedores), Factoring y la compra  de facturas junto a la incorporación de  un proceso de subasta. Las empresas proveedores de productos y servicios podrán financiar su capital de trabajo  a tasas muy competitivas, los inversionistas encontrarán un instrumento de inversión con tasas muy atractivas y las empresas demandantes de productos y servicios podrán realizar el pago de sus cuentas por pagar a través de nuestros bancos aliados 
						</p>
					</div>
				</div>
				<!--centrador-->
				<div id="centrador" class="fadeInDown wow row">
					<!--card izquierda-->
					<div id="izquierda" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
						<center>
							<div class="icon-box-icon">
								<span class="fa-stack fa-lg">
			                      	<i class="fa fa-circle fa-stack-2x"></i>
								 	<i class="fa fa-lock fa-stack-1x fa-inverse" aria-hidden="true"></i>
			                    </span>
								<div style="clear: both"></div>
							</div>
								
							<div class="icon-box-title">
								<h6 id="titulo_clientes1" >Seguridad en tu inversión  (INVERSIONISTA)</h6>
							</div>
							
							<div class="icon-box-content hide" id="icon-box-content-iz">
								<p id="parrafos_clientes1">Altos rendimientos en tu inversión con bajo riesgo ya que contarás con la confirmación de pago por parte de un banco aliado al vencimiento de la factura</p>
							</div>
						</center>
					</div>
					<!--card izquierda end-->
					<!--card centro-->
					<div id="centro" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
						<center>
							<div class="icon-box-icon">
								<span class="fa-stack fa-lg">
			                      	<i class="fa fa-circle fa-stack-2x"></i>
								 	<i class="fa fa-suitcase fa-stack-1x fa-inverse" aria-hidden="true"></i>
			                    </span>
								<div style="clear: both"></div>
							</div>	
							<div class="icon-box-title">
								<h6 id="titulo_clientes2">Extraordinarios beneficios (PROVEEDOR)</h6>
							</div>
								
							<div class="icon-box-content hide" id="icon-box-content-cen">
								<p id="parrafos_clientes2">Procesos agiles para la venta de tus facturas sin necesidad de acudir a una institución financiera, en subasta, aprobarás la tasa de descuento ofertada más conveniente para tu empresa, proveerte de capital de trabajo para potenciar tus nuevas ventas</p>
							</div>
						</center>
					</div>
					<!--card centro end-->
					<!--card derecha -->
					<div id="derecha" class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
						<center>
							<div class="icon-box-icon">
								<span class="fa-stack fa-lg">
			                      	<i class="fa fa-circle fa-stack-2x"></i>
								 	<i class="fa fa-university fa-stack-1x fa-inverse" aria-hidden="true"></i>
			                    </span>
								<div style="clear: both"></div>
							</div>	
							<div class="icon-box-title">
								<h6 id="titulo_clientes3">Gestión de cuentas por pagar</h6>
							</div>
							
							<div class="icon-box-content hide" id="icon-box-content-de">
								<p id="parrafos_clientes3">Como cliente de nuestros Bancos Aliados, tendrás acceso a servicios y productos financieros para la cancelación de tus cuentas por pagar.</p>
							</div>
						</center>
					</div>
					<!--card derecha end-->
					<div style="clear: both"></div>
				</div>
				<!--
				<div class="row centrado boton-nosotros" >
					<div class="col-md-12">
						<div class="text-center"><a class="btn btn-lg btn-circle btn-shadow btn-white btn-ver-mas leer-mas menu1" ></a></div>
					</div>
					<div style="clear: both;"></div>
				</div>
				-->
			<!--centrador end-->					
			</div>
		</div>	
	</section>
	<!-- Quienes somos end-->
	<!-- Parallax de aliados-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax5.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax3">BANCOS ALIADOS</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- BANCOS -->
	<section class="module-sm module-gray aliados">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel clients-carousel" data-carousel-options="{&quot;items&quot;:&quot;4&quot;}">
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-1.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-2.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-7.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-6.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-5.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Parallax de aliados-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax3.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax5">CLIENTES</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--  -->
	<section class="module-sm module-gray aliados">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel clients-carousel" data-carousel-options="{&quot;items&quot;:&quot;4&quot;}">
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-1.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-2.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-7.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-6.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-5.png" alt=""></div>
						<div class="client"><img src="<?=base_url();?>assets/web/images/clients/logo-3.png" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</section>