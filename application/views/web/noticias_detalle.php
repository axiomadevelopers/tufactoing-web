<!-- Wrapper-->
<div class="wrapper">
	<!-- Parallax de servicios-->
	<section class="module parallax bg-dark bg-gradient" data-background="<?=base_url();?>assets/web/images/parallax/parallax6.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 m-auto">
					<div class="text-center texto_parallax">
						<h1 id="parallax6">Noticias</h1>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- fin de parallax -->
	<!-- inicio de section modules noticias -->
	<!-- Blog-->
	<section class="module">
		<div class="container">
			<div class="row">
				<!-- Content-->
				<div class="col-lg-8">
					<!-- Post-->
					<article class="post">
						<div class="post-preview fadeInLeft wow">
							<a href="blog.html#">
								<img id="img_not_desc" src="" alt="">
							</a>
						</div>
						<div class="post-wrapper">
							<div class="post-header fadeInLeft wow">
								<h2 id="titulo_not_desc" class="post-title"></h2>
								<ul class="post-meta h5">
									<li id="fecha_not_desc"></li>
									<li id="usuario_not_desc"></li>
								</ul>
							</div>
							<div class="post-content fadeInLeft wow">
								<p id="des_not_desc"></p>
							</div>
						</div>
					</article>
					<!-- Post end-->
				</div>		
				<!-- Sidebar-->
				<div class="col-lg-4">
					<div class="sidebar">
						<!-- Recent entries widget-->
						<div class="widget-title fadeInDown wow">
							<h3 class="cta-otras-noticias"></h3>
						</div>
						<aside class="widget widget_recent_entries_custom">
							<div class="row">
								<div class="col-lg-12 col-md-6 col-sm-12 col-xs-12  textwidget fadeInUp wow" >
									<p><img id="img_not_1" class="img-news" alt=""></p>
									<h3 id="titulo_not_1"></h3>
									<div class="post-meta h5">
										<label id="fecha_not_1" ></label>
										<label id="usuario_not_1" ></label>
									</div>
									<div class="post-more">
										<a class="leer-mas slug_not_1" id="" href=""></a>
				                	</div>
								</div>
								<div class=" col-lg-12 col-md-6 col-sm-12 col-xs-12 textwidget fadeInUp wow">
									<p><img id="img_not_2" class="img-news" alt=""></p>
									<h3 id="titulo_not_2"></h3>
									<div class="post-meta h5">
										<label id="fecha_not_2" ></label>
										<label id="usuario_not_2" ></label>
									</div>
									<div class="post-more">
										<a class="leer-mas slug_not_2" id="" href=""></a>
				                	</div>
								</div>
							</div>	
						</aside>
					</div>
				</div>
				<input type="hidden" id="idioma_noticia_detalle" value="<?php echo $id_idioma; ?>">
				<input type="hidden" class="slug_noticia" id="" value="<?php echo $slug_noticia; ?>">
				<div class="hide" id="tipo_noticia">...</div>
			</div>
		</div>
	</section>
	<!-- Blog end-->
	<!-- fin de section modeules noticias-->