<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>404 Page Not Found</title>
	<style type="text/css">

	::selection { background-color: #f07746; color: #fff; }
	::-moz-selection { background-color: #f07746; color: #fff; }

	body {
		background-color: #f5f5f5;
		margin: 40px auto;
		max-width: 1024px;
		font: 16px/24px normal "Helvetica Neue", Helvetica, Arial, sans-serif;
		color: #808080;
	}
	.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
	    font-family: Poppins,Arial,serif;
	    line-height: 1.2;
	    font-weight: 700;
	    /* color: #313131; */
	}
	</style>
</head>
<body>
	<div id="container">
		<h1></h1>
		<div id="body">
			<h1 class="h1 m-b-20 " id="titulo_tufactoring" data-wow-delay="0.5s" style="text-align: left;">Tu Factoring | Its Time to win </h1>
			<h2>Error 404 Not Found!</h2>
		</div>
	</div>
</body>
</html>
